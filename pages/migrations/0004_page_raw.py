# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('pages', '0003_page_order'),
    ]

    operations = [
        migrations.AddField(
            model_name='page',
            name='raw',
            field=models.BooleanField(default=False),
        ),
    ]
