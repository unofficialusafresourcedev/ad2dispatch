# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('pages', '0002_auto_20151011_2029'),
    ]

    operations = [
        migrations.AddField(
            model_name='page',
            name='order',
            field=models.PositiveSmallIntegerField(null=True, blank=True),
        ),
    ]
