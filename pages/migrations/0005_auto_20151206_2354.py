# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('pages', '0004_page_raw'),
    ]

    operations = [
        migrations.AlterField(
            model_name='page',
            name='raw',
            field=models.BooleanField(default=False, help_text='Toggles parsing of markdown & display of styling'),
        ),
    ]
