from django.conf.urls import url

from . import views

urlpatterns = [
    url(r'^$', views.upcoming_list, name='upcoming_list'),
    url(r'^driver/$', views.driver, name='driver'),  # TODO Don't hardcode these
    url(r'^dispatcher/$', views.dispatcher, name='dispatcher'),
    url(r'^manage/$', views.manage, name='manage'),
    url(r'^event/(?P<event_id>[0-9]+)/$', views.event, name='event')
]
