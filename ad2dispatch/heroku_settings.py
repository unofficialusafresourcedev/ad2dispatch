import os

DEBUG = bool(os.environ.get('AD2_DEBUG', 'False'))

# Organization Name
ORG_NAME = os.environ.get('AD2_ORG_NAME', 'Offutt Against Drunk Driving')
# Organization Acronym
ORG_ACRONYM = os.environ.get('AD2_ORG_ACRONYM', 'OADD')
# Organization Phone Number: please use international format. +12345678900
ORG_PHONE = os.environ.get('AD2_ORG_PHONE', '+14022942233')
# Organization Phone Number: friendly format.
ORG_PHONE_DISPLAY = os.environ.get('AD2_ORG_PHONE_DISPLAY', '294-AADD (2233)')
# Disclaimer [Required by most *ADD orgs]
DISCLAIMER = os.environ.get('AD2_ORG_DISCLAIMER', '''AS REQUIRED BY AFI 34-223 WE ARE REQUIRED TO INFORM YOU THIS IS A PRIVATE ORGANIZATION.
 IT IS NOT A PART OF THE DEPARTMENT OF DEFENSE OR ANY OF ITS COMPONENTS AND IT HAS NO GOVERNMENTAL STATUS.''')

# Social links
# Use just the name with no markup.
# Ex: OffuttADD, not @OffuttADD, and not a URL.
FACEBOOK = os.environ.get('AD2_ORG_FACEBOOK', 'OffuttADD')
TWITTER = os.environ.get('AD2_ORG_TWITTER', 'OffuttADD')

# Google Maps API key
# https://developers.google.com/maps/documentation/javascript/get-api-key
MAPS_KEY = os.environ.get('AD2_MAPS_KEY', '')

# Time zone
# https://docs.djangoproject.com/en/dev/ref/settings/#time-zone
TIME_ZONE = os.environ.get('AD2_TIMEZONE', 'America/Chicago')

# Email settings
# https://docs.djangoproject.com/en/dev/ref/settings/#email-host
EMAIL_HOST = os.environ.get('AD2_EMAIL_HOST')
EMAIL_PORT = os.environ.get('AD2_EMAIL_PORT', 587)
EMAIL_USE_TLS = os.environ.get('AD2_EMAIL_USE_TLS', True)
EMAIL_HOST_USER = os.environ.get('AD2_EMAIL_HOST_USER')
EMAIL_HOST_PASSWORD = os.environ.get('AD2_EMAIL_HOST_PASSWORD')
DEFAULT_FROM_EMAIL = os.environ.get('AD2_EMAIL_DEFAULT_FROM')

# !!**!! SECURITY WARNING: keep the secret key used in production secret! !!**!!
# Security Key
# https://docs.djangoproject.com/en/dev/ref/settings/#secret-key
SECRET_KEY = os.environ.get('AD2_SECRET_KEY')

try:
    from ad2dispatch.heroku_settings_testing import *
except ImportError:
    pass
