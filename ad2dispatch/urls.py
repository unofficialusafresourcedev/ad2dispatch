from django.conf.urls import include, url
from django.contrib import admin
from django.contrib.auth.views import login, logout, password_change
from axes.decorators import watch_login

urlpatterns = [
    url(r'^admin/', include(admin.site.urls)),
    url(r'^accounts/profile/', include('userprofiles.urls', namespace="userprofiles")),
    url(r'^accounts/', include('registration.backends.default.urls')),
    url(r'^volunteer/', include('events.urls', namespace="events")),
    # url(r'^dispatch/', include('dispatch.urls', namespace="dispatch")),
    url(r'^news/', include('news.urls', namespace="news")),
    url(r'^', include('pages.urls', namespace="pages")),
]
