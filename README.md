# ad2dispatch
A web-based platform for managing and operating '* Against Drunk Driving' programs.
Think volunteer-org Uber/Lyft.

## How do I use it?

Currently it is not ready for public use, although if you would like to contribute to development, feel free to build it yourself and submit pull requests.

## License

[Licensed Under Apache 2.0](https://www.apache.org/licenses/LICENSE-2.0)
