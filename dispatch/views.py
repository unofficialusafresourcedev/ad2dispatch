from __future__ import print_function

from django.contrib.auth.decorators import login_required
from django.shortcuts import render


@login_required
def dashboard(request):
    context = {
        'temp': True
    }
    return render(request, 'dashboard.html', context)
