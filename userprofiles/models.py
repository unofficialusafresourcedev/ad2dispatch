from django.contrib.auth.models import User
from django.db import models


class Volunteer(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    service = models.CharField(max_length=24, null=True, blank=True, default='USAF')
    rank = models.CharField(max_length=16, null=True, blank=True)
    phone_number = models.CharField(max_length=14, null=True)
    phone_number_2 = models.CharField(max_length=14, null=True, blank=True)
    vehicle_desc = models.CharField(max_length=128, null=True)
    ready = models.BooleanField(default=False, editable=True,
                                help_text='If driver has marked themselves as \'ready\' for the event duration.')
    dispatched = models.BooleanField(default=False, editable=False)
    council_pos = models.CharField(max_length=24, null=True, blank=True)
    sup_name = models.CharField(max_length=64, null=True)
    sup_phone = models.CharField(max_length=14, null=True)
    location = models.DecimalField(max_digits=9, decimal_places=6, null=True, blank=True)

    def is_populated(self):
        if self.user is not None and \
                        self.phone_number is not None and \
                        self.vehicle_desc is not None and \
                        self.sup_name is not None and \
                        self.sup_phone is not None:
            return True
        return False

    def is_ready(self):
        if self.ready and not self.dispatched:
            return True
        return False
