# -*- coding: utf-8 -*-
# Generated by Django 1.10 on 2016-09-03 05:25
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('userprofiles', '0001_initial'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='volunteer',
            name='user',
        ),
        migrations.DeleteModel(
            name='Volunteer',
        ),
    ]
