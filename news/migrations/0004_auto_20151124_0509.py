# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import django.utils.datetime_safe
from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('news', '0003_auto_20151123_0459'),
    ]

    operations = [
        migrations.AlterField(
            model_name='article',
            name='created_date',
            field=models.DateTimeField(default=django.utils.datetime_safe.datetime.now),
        ),
    ]
